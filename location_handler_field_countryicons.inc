<?php

class location_handler_field_countryicons extends location_handler_field_location_country {

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['style']['#options']['countryflag'] = t('Country flag');
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    $style = $this->options['style'];

    if($value && $style == 'countryflag') {
      return theme_countryicons_icon($value);
    } else {
      return parent::render($values);
    }
  }

}


