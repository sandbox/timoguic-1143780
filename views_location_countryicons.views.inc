<?php

/*
function grut_tools_views_data() {

  $data['node']['created'] = array(
    'field' => array('handler' => 'grut_tools_handler_field_date'),
    'filter' => array('handler' => 'views_handler_filter_date'),
    'sort' => array('handler' => 'views_handler_sort_date'),
  );

  return $data;
}
*/

function views_location_countryicons_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    foreach ($config as $item => $item_config) {
      if (isset($item_config['field']) && $item_config['field']['handler'] == 'location_handler_field_location_country') {
        $data[$table][$item]['field']['handler'] = 'location_handler_field_countryicons';
      }
    }
  }
}

function views_location_countryicons_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_location_countryicons'),
    ),
    'handlers' => array(
      'location_handler_field_countryicons' => array(
        'parent' => 'location_handler_field_location_country',
      ),
    ),
  );
}
